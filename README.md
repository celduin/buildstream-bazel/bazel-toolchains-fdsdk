Freedesktop SDK toolchains for Bazel
=================

# Prerequisites

* Install BuildStream and the experimental plugins:
```
# git clone -n https://gitlab.com/BuildStream/buildstream.git buildstream && \
cd buildstream && \
git checkout 1.93.4-53-g7ff7fb5cde1b491c2e2a321b705d695f48980cfe
# pip install -e buildstream
# pip install bst-plugins-experimental==1.93.4
```
* Install Bazel:
```
# curl https://github.com/bazelbuild/bazel/releases/download/3.1.0/bazel-3.1.0-installer-linux-x86_64.sh > bazel-3.1.0.sh && \
chmod +x bazel-3.1.0.sh && \
./bazel-3.1.0.sh --prefix=/root/.local
```

# Building the hello-world demo

## for an `x86_64` target

* The current version of Freedesktop SDK used builds with libc 2.30. If your
  system's libc is older than this you won't be able to build with Bazel.

* Edit the `project_dir` value for `bst_element` in `./tests/WORKSPACE`:

```
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")

git_repository(
  name = "rules_buildstream",
  branch = "master",
  remote = "https://gitlab.com/celduin/buildstream-bazel/rules_buildstream.git",
)

load("@rules_buildstream//bst:defs.bzl", "bst_element")

local_repository(
  name = "fdsdk_toolchain_repo",
  path = "..",
)

bst_element(
  name = "bazel-toolchain",
  element = "toolchain-x86_64-deploy.bst",
  timeout = 14400,
  project_dir = "<absolute path to bazel-toolchains-fdsdk directory>",
)

load("@fdsdk_toolchain_repo//toolchain:gen_build_defs.bzl", "gen_build_defs")

gen_build_defs(
  name = "fdsdk",
  arch = "x86_64",
  archive = "@bazel-toolchain//bazel-toolchain",
  parent_repo = "@fdsdk_toolchain_repo",
  build_template_file = "@fdsdk_toolchain_repo//toolchain:BUILD.in",
  wrapper_template_file = "@fdsdk_toolchain_repo//toolchain:wrapper.in",
)

register_toolchains("@fdsdk//:cc-toolchain")
```

* Rewrite `test/.bazelrc` to contain only:

```
build:fdsdk --crosstool_top=@fdsdk//:fdsdk
build:fdsdk --cpu=x86_64
```

* Build the hello-world target from `./tests`:

```
$ bazel build --config=fdsdk //main:hello-world
```

* Run the hello-world executable from `./tests`:

```
$ bazel run --config=fdsdk //main:hello-world
```

## for an `aarch64` target

* Edit `./tests/WORKSPACE`:

```
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")

git_repository(
    name = "rules_buildstream",
    branch = "master",
    remote = "https://gitlab.com/celduin/buildstream-bazel/rules_buildstream.git",
)

load("@rules_buildstream//bst:defs.bzl", "bst_element")

local_repository(
  name = "fdsdk_toolchain_repo",
  path = "..",
)

bst_element(
  name = "bazel-toolchain",
  element = "toolchain-aarch64-deploy.bst",
  timeout = 14400,
  project_dir = <absolute path to bazel-toolchains-fdsdk project directory>,
)

load("@fdsdk_toolchain_repo//toolchain:gen_build_defs.bzl", "gen_build_defs")

gen_build_defs(
  name = "fdsdk",
  arch = "aarch64",
  archive = "@bazel-toolchain//bazel-toolchain",
  parent_repo = "@fdsdk_toolchain_repo",
  build_template_file = "@fdsdk_toolchain_repo//toolchain:BUILD.in",
  wrapper_template_file = "@fdsdk_toolchain_repo//toolchain:wrapper.in",
)

register_toolchains("@fdsdk//:cc-toolchain")
```
* Rewrite `test/.bazelrc` to contain only:

```
build:fdsdk --crosstool_top=@fdsdk//:fdsdk
build:fdsdk --cpu=aarch64
```

* Build the hello-world executable

```
$ bazel build --config=fdsdk //main:hello-world
```

On an aarch64 platform the binary can be run the same way
```
$ bazel run --config=fdsdk //main:hello-world
```

# Using the toolchain in other projects

* Follow the instructions in the previous sections, except now pull the bazel-toolchain project in as an external repository instead of a local one:
```
git_repository(
  name = "fdsdk_toolchain_repo",
  tag = "0.1.1",
  remote = "https://gitlab.com/Celduin/buildstream-bazel/bazel-toolchains-fdsdk",
)
```
An example of this can be found in a fork of the [abseil-hello project](https://gitlab.com/celduin/buildstream-bazel/abseil-hello-fork/-/blob/master/bazel-hello/WORKSPACE).

# Troubleshooting

After installing the WORKSPACE files, use the toolchain by adding the
environment variable `BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1`
(throws up errors if it fails to find the toolchain), and including in your
command-line options `--crosstool_top=@fdsdk//:fdsdk --cpu=x86_64` (or aarch64).

e.g. `BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1 bazel build --crosstool_top=@fdsdk//:fdsdk --cpu=x86_64 //main:...`
