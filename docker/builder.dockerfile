FROM ubuntu:20.04

RUN apt -y update
RUN apt -y install \
  gcc \
  wget \
  git \
  unzip \
  curl

# Install bazel
ARG BAZEL_VER=3.1.0
RUN wget "https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VER}/bazel-${BAZEL_VER}-installer-linux-x86_64.sh"
RUN chmod +x "bazel-${BAZEL_VER}-installer-linux-x86_64.sh"
RUN "./bazel-${BAZEL_VER}-installer-linux-x86_64.sh" --prefix=/root/.local
