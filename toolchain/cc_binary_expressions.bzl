# Copyright 2019 Codethink Ltd. All rights reserved.
#
# This work is licensed under the terms of the MIT license (see LICENSE).

"""A collection of globbing patterns for finding binaries in the toolchain"""

_TOOLCHAIN_PREFIX = "usr/lib/sdk/toolchain-*"
_PREFIX = "bin"

AR_BINARY = "*-unknown-linux-gnu-gcc-ar"
CPP_BINARY = "cpp"
DWP_BINARY = "dwp"
GCC_BINARY = "*-unknown-linux-gnu-gcc"
GCOV_BINARY = "gcov"
LD_BINARY = "ld"
NM_BINARY = "*-unknown-linux-gnu-gcc-nm"
OBJCOPY_BINARY = "objcopy"
OBJDUMP_BINARY = "objdump"
STRIP_BINARY = "strip"

AR_PATTERN      = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + AR_BINARY
CPP_PATTERN     = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + CPP_BINARY
DWP_PATTERN     = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + DWP_BINARY
GCC_PATTERN     = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + GCC_BINARY
GCOV_PATTERN    = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + GCOV_BINARY
LD_PATTERN      = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + LD_BINARY
NM_PATTERN      = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + NM_BINARY
OBJCOPY_PATTERN = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + OBJCOPY_BINARY
OBJDUMP_PATTERN = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + OBJDUMP_BINARY
STRIP_PATTERN   = _TOOLCHAIN_PREFIX + "/" + _PREFIX + "/" + STRIP_BINARY

ALL_PATTERN = "**/*"
EXCLUDE_MAN_PATTERN = "usr/share/man/**/*"
EXCLUDE_BUILD_FILE_PATTERN = "BUILD.bazel"

LD_LIBRARY_PATHS = [
    _TOOLCHAIN_PREFIX + "/lib",
    _TOOLCHAIN_PREFIX + "/lib/*-linux-gnu",
    _TOOLCHAIN_PREFIX + "/*-unknown-linux-gnu/lib",
]
