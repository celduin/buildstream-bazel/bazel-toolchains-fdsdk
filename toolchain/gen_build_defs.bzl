load(":cc_binary_expressions.bzl",
     "AR_PATTERN",
     "CPP_PATTERN",
     "DWP_PATTERN",
     "GCC_PATTERN",
     "GCOV_PATTERN",
     "LD_PATTERN",
     "NM_PATTERN",
     "OBJCOPY_PATTERN",
     "OBJDUMP_PATTERN",
     "STRIP_PATTERN",
     "LD_LIBRARY_PATHS",
)

def _resolve_glob(ctx, pattern, cwd):
    result = ctx.execute(["sh", "-c", "ls -d " + pattern], working_directory = cwd)
    if result.return_code != 0:
        ctx.execute(["echo", "from: {} to: {} {} {}".format(pattern, result.return_code, result.stdout, result.stderr)], quiet=False)
        fail(msg=str(result.return_code), attr=result.stderr)
    output = result.stdout[:-1] # strip trailing newline
    return output

_binary_patterns = [
    ("ar", AR_PATTERN),
    ("cpp",  CPP_PATTERN),
    ("dwp",  DWP_PATTERN),
    ("gcc",  GCC_PATTERN),
    ("gcov",  GCOV_PATTERN),
    ("ld",  LD_PATTERN),
    ("nm",  NM_PATTERN),
    ("objcopy",  OBJCOPY_PATTERN),
    ("objdump",  OBJDUMP_PATTERN),
    ("strip",  STRIP_PATTERN),
]

def _gen_build_defs_impl(repository_ctx):
    # Generate BUILD file from a template.
    parent_label = repository_ctx.attr.parent_repo
    # ensure we get the archive relative to this repo
    archive_label = parent_label.relative(str(repository_ctx.attr.archive))

    # to avoid passing the default name attr of Label, recompose from workspace and package
    parent_repo = str(parent_label.workspace_name)
    archive_repo = str(archive_label.workspace_name) + "//" + str(archive_label.package)

    archive_rel_path = "{}/{}".format(archive_label.workspace_name, archive_label.package)
    archive_path = str(repository_ctx.path("../" + archive_rel_path))

    # write BUILD file to '@fdsdk' repo
    template = repository_ctx.attr.build_template_file
    build_file = repository_ctx.path("BUILD")
    repository_ctx.template(build_file, template, {
        "{ARCH}": repository_ctx.attr.arch,
        "{ARCHIVE}": archive_repo,
        "{PARENT_REPOSITORY}": parent_repo,
    })

    # Generate LD_LIBRARY_PATH
    ld_processed_paths = [_resolve_glob(repository_ctx, p, archive_path) for p in LD_LIBRARY_PATHS]
    # Split multiple matches
    ld_processed_paths = [split for multiple in ld_processed_paths for split in multiple.split('\n')]
    ld_library_paths = ":".join(["external/" + archive_rel_path + "/" + p for p in ld_processed_paths])

    # Generate wrapper scripts
    template_path = repository_ctx.attr.wrapper_template_file
    for binary_name, pattern in _binary_patterns:
        wrapper_filename = binary_name + "_wrapper.sh"
        wrapper_file = repository_ctx.path(wrapper_filename)
        binary_path = _resolve_glob(repository_ctx, pattern, archive_path)
        repository_ctx.template(wrapper_file, template_path, {
            "{REPOSITORY}": "{}/{}".format(archive_label.workspace_name, archive_label.package),
            "{BINARY}": binary_path,
            "{LD_LIBRARY_PATH}": ld_library_paths,
        })

_gen_build_defs_attrs = {
    "arch": attr.string(
        mandatory = True,
        doc = "The architecture of the imported toolchain (e.g. x86_64)",
        values=["x86_64", "aarch64"]
    ),
    "archive": attr.label(
        mandatory = True,
        doc = "The label of the repository that contains the toolchain",
    ),
    "build_template_file": attr.label(
        mandatory = True,
        doc = "The label of the build file template to apply, applying substitutions to '{ARCH}'",
        allow_single_file = True,
    ),
    "parent_repo": attr.label(
        mandatory = True,
        doc = "The label of the repository that provides this rule", # We need this information because native.repository_name() doesn't work on repository rules
    ),
    "wrapper_template_file": attr.label(
        mandatory = True,
        doc = "The label of the wrapper file template to apply, applying substitutions to '{LD_LIBRARY_PATH}', '{REPOSITORY}' and '{BINARY}'",
        allow_single_file = True,
    ),
}

gen_build_defs = repository_rule(
    implementation = _gen_build_defs_impl,
    attrs = _gen_build_defs_attrs,
)
